import { UUIDV4 } from 'sequelize';
import { DataTypes, Model } from 'sequelize';
import db from './db/sequelize';

interface ContratoAttributes {
    id: number;
    numerocontrato: number;
    empresa: string;
    nomedevedor: string;
    statuscontrato: boolean;
    createdAt?: Date;
    updatedAt?: Date;
}

export default class ContratoInstance extends Model<ContratoAttributes> {}

ContratoInstance.init({
    id: {
        type: DataTypes.UUID,
        defaultValue: UUIDV4,
        autoIncrement: true,
        primaryKey: true,
    },
    numerocontrato: {
        type: DataTypes.INTEGER,
        allowNull: false,
        unique: true,
    },
    empresa: {
        type: DataTypes.STRING,
        allowNull: false
    },
    nomedevedor: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    statuscontrato: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        field: 'createdat',
        defaultValue: DataTypes.NOW,
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        field: 'updatedat',
        defaultValue: DataTypes.NOW,
    }},
	{
		sequelize: db,
		tableName: 'contratos'
	}
);