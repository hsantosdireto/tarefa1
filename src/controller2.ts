import { Request, Response } from 'express'
import ContratoInstance  from './contratoModel'
//import contrModel from '../contratoModel2';

	class listContratos {
    async show(req: Request, res: Response)/*: Promise<Response>*/{

      const contratosList = await ContratoInstance.findAll();
  
      if(!contratosList) {
        throw new Error('Contratos não localizados');
      }
      return res.json(contratosList);
    }
  }

export default new listContratos();