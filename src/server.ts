
import express = require("express");
import contratoRoute from './route2'
import { Request, Response, NextFunction} from 'express'
import cors from 'cors'
import sequelize from './db/sequelize';

const db = sequelize.authenticate().then(()=>{
	console.log('Conexão com o banco de dados realizada com sucesso')
  }).catch(error => {
	console.log('Falha na conexão com o banco de dados')
  })

const app = express();
const port = 3041;

app.use(express.json());

//Permissão de Acesso

app.use((req: Request, res: Response, next: NextFunction)=>{
	app.use(cors())
	res.header('Access-Control-Allow-Origin', '*')
	next();
})

//Reads

//Pegar Tudo
app.get('/retorno', async (req: Request, res: Response)=>{
	try {
        const response = await sequelize.query('SELECT * FROM public.contratos ORDER BY id ASC');
        return res.status(200).json(response);
    } catch (e) {
        console.log(e);
        return res.status(500).json('Internal Server error');
    }
})
//Pegar somente os números de contrato
app.get('/numerocontrato', async (req: Request, res: Response)=>{
	try {
        const response = await sequelize.query('SELECT numerocontrato FROM public.contratos ORDER BY id ASC');
        return res.status(200).json(response);
    } catch (e) {
        console.log(e);
        return res.status(500).json('Internal Server error');
    }
})
//Pegar somente as nomes da empresas
app.get('/empresa', async (req: Request, res: Response)=>{
	try {
        const response = await sequelize.query('SELECT empresa FROM public.contratos ORDER BY id ASC');
        return res.status(200).json(response);
    } catch (e) {
        console.log(e);
        return res.status(500).json('Internal Server error');
    }
})
//Pegar somente os nomes dos devedores
app.get('/nomedevedor', async (req: Request, res: Response)=>{
	try {
        const response = await sequelize.query('SELECT nomedevedor FROM public.contratos ORDER BY id ASC');
        return res.status(200).json(response);
    } catch (e) {
        console.log(e);
        return res.status(500).json('Internal Server error');
    }
})
//Pegar somente os status
app.get('/status', async (req: Request, res: Response)=>{
	try {
        const response = await sequelize.query('SELECT statuscontrato FROM public.contratos ORDER BY id ASC');
        return res.status(200).json(response);
    } catch (e) {
        console.log(e);
        return res.status(500).json('Internal Server error');
    }
})

//Post

app.post('/criar', async (req: Request, res: Response)=> {
	const response = await sequelize.query(`INSERT INTO public.contratos (numerocontrato, empresa, nomedevedor, statuscontrato) values ($1, $2, $3, $4)`);
    const values = [req.body.numerocontrato, req.body.empresa, req.body.nomedevedor, req.body.statuscontrato]
    return res.json(response);
});

//Update

app.put('/update/:id', async (req: Request, res: Response)=> {
    const id = parseInt(req.params.id);
    const { numerocontrato, empresa, nomedevedor, statuscontrato } = req.body;
    const response = await sequelize.query(`UPDATE public.contratos (numerocontrato, empresa, nomedevedor, statuscontrato) VALUES ($1, $2, $3, $4) WHERE id = $5)`);
	const values = [req.body.numerocontrato, req.body.empresa, req.body.nomedevedor, req.body.statuscontrato]
    res.json(`User ${id} updated Successfully`);
});

//Delete

app.delete('/delete/:id', async (req: Request, res: Response)=> {
    const id = parseInt(req.params.id);
    const response = await sequelize.query(`DELETE FROM users where id = ${id}`);
    res.json(`User ${id} deleted Successfully`);
});

//Tentativa de Paginação

/*app.get('readPagination', async (req: Request, res: Response) {
	try {
		const limit = req.query?.limit as unknown as number | undefined;
		const offset = req.query?.offset as unknown as number | undefined;

		const records = await sequelize({ where: {}, limit, offset }); //buscando tudo
		return res.json(records);
	} catch (error) {
		return res.json({ msg: 'Falha ao ler', status: 500, route: '/read' });
	}
})*/

//Rota principal

app.use('/contratos', contratoRoute);

app.listen(port, () => {
	console.log('Servidor iniciado na porta: ' + port);
});