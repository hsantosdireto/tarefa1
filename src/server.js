"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var express = require("express");
var route2_1 = require("./route2");
var cors_1 = require("cors");
var sequelize_1 = require("./db/sequelize");
var db = sequelize_1["default"].authenticate().then(function () {
    console.log('Conexão com o banco de dados realizada com sucesso');
})["catch"](function (error) {
    console.log('Falha na conexão com o banco de dados');
});
var app = express();
var port = 3040;
app.use(express.json());
//Permissão de Acesso
app.use(function (req, res, next) {
    app.use(cors_1["default"]());
    res.header('Access-Control-Allow-Origin', '*');
    next();
});
//Reads
//Pegar Tudo
app.get('/retorno', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var response, e_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, sequelize_1["default"].query('SELECT * FROM public.contratos ORDER BY id ASC')];
            case 1:
                response = _a.sent();
                return [2 /*return*/, res.status(200).json(response)];
            case 2:
                e_1 = _a.sent();
                console.log(e_1);
                return [2 /*return*/, res.status(500).json('Internal Server error')];
            case 3: return [2 /*return*/];
        }
    });
}); });
//Pegar somente os números de contrato
app.get('/numerocontrato', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var response, e_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, sequelize_1["default"].query('SELECT numerocontrato FROM public.contratos ORDER BY id ASC')];
            case 1:
                response = _a.sent();
                return [2 /*return*/, res.status(200).json(response)];
            case 2:
                e_2 = _a.sent();
                console.log(e_2);
                return [2 /*return*/, res.status(500).json('Internal Server error')];
            case 3: return [2 /*return*/];
        }
    });
}); });
//Pegar somente as nomes da empresas
app.get('/empresa', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var response, e_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, sequelize_1["default"].query('SELECT empresa FROM public.contratos ORDER BY id ASC')];
            case 1:
                response = _a.sent();
                return [2 /*return*/, res.status(200).json(response)];
            case 2:
                e_3 = _a.sent();
                console.log(e_3);
                return [2 /*return*/, res.status(500).json('Internal Server error')];
            case 3: return [2 /*return*/];
        }
    });
}); });
//Pegar somente os nomes dos devedores
app.get('/nomedevedor', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var response, e_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, sequelize_1["default"].query('SELECT nomedevedor FROM public.contratos ORDER BY id ASC')];
            case 1:
                response = _a.sent();
                return [2 /*return*/, res.status(200).json(response)];
            case 2:
                e_4 = _a.sent();
                console.log(e_4);
                return [2 /*return*/, res.status(500).json('Internal Server error')];
            case 3: return [2 /*return*/];
        }
    });
}); });
//Pegar somente os status
app.get('/status', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var response, e_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, sequelize_1["default"].query('SELECT statuscontrato FROM public.contratos ORDER BY id ASC')];
            case 1:
                response = _a.sent();
                return [2 /*return*/, res.status(200).json(response)];
            case 2:
                e_5 = _a.sent();
                console.log(e_5);
                return [2 /*return*/, res.status(500).json('Internal Server error')];
            case 3: return [2 /*return*/];
        }
    });
}); });
//Post
app.post('/criar', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var response, values;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, sequelize_1["default"].query("INSERT INTO public.contratos (numerocontrato, empresa, nomedevedor, statuscontrato) values ($1, $2, $3, $4)")];
            case 1:
                response = _a.sent();
                values = [req.body.numerocontrato, req.body.empresa, req.body.nomedevedor, req.body.statuscontrato];
                return [2 /*return*/, res.json(response)];
        }
    });
}); });
//Update
app.put('/update/:id', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var id, _a, numerocontrato, empresa, nomedevedor, statuscontrato, response, values;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                id = parseInt(req.params.id);
                _a = req.body, numerocontrato = _a.numerocontrato, empresa = _a.empresa, nomedevedor = _a.nomedevedor, statuscontrato = _a.statuscontrato;
                return [4 /*yield*/, sequelize_1["default"].query("UPDATE public.contratos (numerocontrato, empresa, nomedevedor, statuscontrato) VALUES ($1, $2, $3, $4) WHERE id = $5)")];
            case 1:
                response = _b.sent();
                values = [req.body.numerocontrato, req.body.empresa, req.body.nomedevedor, req.body.statuscontrato];
                res.json("User " + id + " updated Successfully");
                return [2 /*return*/];
        }
    });
}); });
//Delete
app["delete"]('/delete/:id', function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var id, response;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                id = parseInt(req.params.id);
                return [4 /*yield*/, sequelize_1["default"].query("DELETE FROM users where id = " + id)];
            case 1:
                response = _a.sent();
                res.json("User " + id + " deleted Successfully");
                return [2 /*return*/];
        }
    });
}); });
//Tentativa de Paginação
/*app.get('readPagination', async (req: Request, res: Response) {
    try {
        const limit = req.query?.limit as unknown as number | undefined;
        const offset = req.query?.offset as unknown as number | undefined;

        const records = await sequelize({ where: {}, limit, offset }); //buscando tudo
        return res.json(records);
    } catch (error) {
        return res.json({ msg: 'Falha ao ler', status: 500, route: '/read' });
    }
})*/
//Rota principal
app.use('/contratos', route2_1["default"]);
app.listen(port, function () {
    console.log('Servidor iniciado na porta: ' + port);
});
