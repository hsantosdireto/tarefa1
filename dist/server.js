"use strict";
exports.__esModule = true;
var express = require("express");
var route2_1 = require("../src/route2");
var sequelize_1 = require("../src/db/sequelize");
var db = sequelize_1["default"].authenticate().then(function () {
    console.log('Conexão com o banco de dados realizada com sucesso');
})["catch"](function (error) {
    console.log('Falha na conexão com o banco de dados');
});
var app = express();
var port = 3021;
app.get('/', function (req, res) {
    return res.send('Bem vindo ao menu de contrato!');
});
app.use(express.json());
app.use('/contratos', route2_1["default"]);
app.listen(port, function () {
    console.log('Servidor iniciado na porta: ' + port);
});
