"use strict";
exports.__esModule = true;
var express = require("express");
var validator_1 = require("../src/validator");
//import Middleware from '../../middleware';
var controller_1 = require("../src/controller");
var router = express.Router();
router.post('/create', validator_1["default"].checkCreateContrato(), 
//Middleware.handleValidationError,
controller_1["default"].create);
router.get('/read', validator_1["default"].checkReadContrato(), 
//Middleware.handleValidationError,
controller_1["default"].readPagination);
router.get('/read/:id', validator_1["default"].checkIdParam(), 
//Middleware.handleValidationError,
controller_1["default"].readByID);
router.put('/update/:id', validator_1["default"].checkIdParam(), 
//Middleware.handleValidationError,
controller_1["default"].update);
router["delete"]('/delete/:id', validator_1["default"].checkIdParam(), 
//Middleware.handleValidationError,
controller_1["default"]["delete"]);
exports["default"] = router;
