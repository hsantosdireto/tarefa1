"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.ContratoInstance = void 0;
var sequelize_1 = require("sequelize");
var sequelize_2 = require("sequelize");
var sequelize_3 = require("../src/db/sequelize");
var ContratoInstance = /** @class */ (function (_super) {
    __extends(ContratoInstance, _super);
    function ContratoInstance() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return ContratoInstance;
}(sequelize_2.Model));
exports.ContratoInstance = ContratoInstance;
ContratoInstance.init({
    id: {
        type: sequelize_2.DataTypes.UUID,
        defaultValue: sequelize_1.UUIDV4,
        autoIncrement: true,
        primaryKey: true
    },
    numerocontrato: {
        type: sequelize_2.DataTypes.INTEGER,
        allowNull: false,
        unique: true
    },
    empresa: {
        type: sequelize_2.DataTypes.STRING,
        allowNull: false
    },
    nomedevedor: {
        type: sequelize_2.DataTypes.STRING,
        allowNull: false
    },
    statuscontrato: {
        type: sequelize_2.DataTypes.BOOLEAN,
        allowNull: false
    },
    createdAt: {
        type: sequelize_2.DataTypes.DATE,
        allowNull: false,
        field: 'createdat',
        defaultValue: sequelize_2.DataTypes.NOW
    },
    updatedAt: {
        type: sequelize_2.DataTypes.DATE,
        allowNull: false,
        field: 'updatedat',
        defaultValue: sequelize_2.DataTypes.NOW
    }
}, {
    sequelize: sequelize_3["default"],
    tableName: 'contratos'
});
