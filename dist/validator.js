"use strict";
exports.__esModule = true;
var express_validator_1 = require("express-validator");
var ContratoValidator = /** @class */ (function () {
    function ContratoValidator() {
    }
    ContratoValidator.prototype.checkCreateContrato = function () {
        return [
            express_validator_1.body('id')
                .optional()
                .isUUID(4)
                .withMessage('O valor deve ser UUID v4'),
            express_validator_1.body('title')
                .notEmpty()
                .withMessage('O valor do título não deve estar vazio'),
            express_validator_1.body('completed')
                .optional()
                .isBoolean()
                .withMessage('O valor deve ser booleano')
                .isIn([0, false])
                .withMessage('O valor deve ser 0 ou falso'),
        ];
    };
    ContratoValidator.prototype.checkReadContrato = function () {
        return [
            express_validator_1.query('limit')
                .notEmpty()
                .withMessage('O limite da consulta não deve estar vazio')
                .isInt({ min: 1, max: 10 })
                .withMessage('O valor limite deve ser um número e entre 1-10'),
            express_validator_1.query('offset')
                .optional()
                .isNumeric()
                .withMessage('O valor deve ser numérico'),
        ];
    };
    ContratoValidator.prototype.checkIdParam = function () {
        return [
            express_validator_1.param('id')
                .notEmpty()
                .withMessage('O valor não deve estar vazio')
                .isUUID(4)
                .withMessage('O valor não deve estar vazio'),
        ];
    };
    return ContratoValidator;
}());
exports["default"] = new ContratoValidator();
