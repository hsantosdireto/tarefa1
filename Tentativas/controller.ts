import express, {Request, Response} from 'express';

import { v4 as uuidv4 } from 'uuid';
import ContratoInstance from '../src/contratoModel';

class ContratoController {
	async create(req: Request, res: Response) { //Criação da tabela
		const id = uuidv4();
		try {
			const record = await ContratoInstance.create({ ...req.body, id });
			return res.json({ record, msg: 'Sucesso na criação do Contrato' });
		} catch (error) {
			return res.json({ msg: 'Falha na criação', status: 500, route: '/create' }); //retorno do erro e onde
		}
	}

	async readPagination(req: Request, res: Response) {
		try {
			const limit = req.query?.limit as unknown as number | undefined;
			const offset = req.query?.offset as unknown as number | undefined;

			const records = await ContratoInstance.findAll({ where: {}, limit, offset }); //buscando tudo
			return res.json(records);
		} catch (error) {
			return res.json({ msg: 'Falha ao ler', status: 500, route: '/read' });
		}
	}
	async readByID(req: Request, res: Response) {
		try {
			const { id } = req.params;
			const record = await ContratoInstance.findOne({ where: { id } });
			return res.json(record);
		} catch (error) {
			return res.json({ msg: 'Falha ao ler', status: 500, route: '/read/:id' });
		}
	}
	async update(req: Request, res: Response) {
		try {
			const { id } = req.params;
			const record = await ContratoInstance.findOne({ where: { id } });

			if (!record) {
				return res.json({ msg: 'Não é possível encontrar o registro existente' });
			}
		} catch (error) {
			return res.json({
				msg: 'Falha ao ler',
				status: 500,
				route: '/update/:id',
			});
		}
	}
	async delete(req: Request, res: Response) {
		try {
			const { id } = req.params;
			const record = await ContratoInstance.findOne({ where: { id } });

			if (!record) {
				return res.json({ msg: 'Não é possível encontrar o registro existente' });
			}

			const deletedRecord = await record.destroy();
			return res.json({ record: deletedRecord });
		} catch (error) {
			return res.json({
				msg: 'Falha ao ler',
				status: 500,
				route: '/delete/:id',
			});
		}
	}
}

export default new ContratoController();