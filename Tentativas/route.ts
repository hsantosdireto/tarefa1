import * as express from 'express';
import ContratoValidator from './validator';
import ContratoController from './controller';
import ContratoInstance  from '../src/contratoModel';

const router = express.Router();

router.post(
	'/create',
	ContratoValidator.checkCreateContrato(),
	ContratoController.create
);

router.get(
	'/read',
	ContratoValidator.checkReadContrato(),
	ContratoController.readPagination
);

router.get(
	'/read/:id',
	ContratoValidator.checkIdParam(),
	ContratoController.readByID
);

router.put(
	'/update/:id',
	ContratoValidator.checkIdParam(),
	ContratoController.update
);

router.delete(
	'/delete/:id',
	ContratoValidator.checkIdParam(),
	ContratoController.delete
);

export default router;