
import { body, param, query } from 'express-validator';

class ContratoValidator {
	checkCreateContrato() {
		return [
			body('id')
				.optional()
				.isUUID(4)
				.withMessage('O valor deve ser UUID v4'),
			body('numeroContrato')
				.notEmpty()
				.withMessage('O número do contrato não deve estar vazio'),
            body('numeroContrato')
				.notEmpty()
				.withMessage('O valor do contrato não deve estar vazio'),
            body('empresa')
				.notEmpty()
				.withMessage('O nome da empresa não deve estar vazio'),
            body('nomeDevedor')
				.notEmpty()
				.withMessage('O nome do devedor não deve estar vazio'),
            body('statusContrato')
				.notEmpty()
				.withMessage('O valor do status não deve estar vazio'),
			body('completed')
				.optional()
				.isBoolean()
				.withMessage('O valor deve ser booleano')
				.isIn([0, false])
				.withMessage('O valor deve ser 0 ou falso'),
		];
	}
	checkReadContrato() {
		return [
			query('limit')
				.notEmpty()
				.withMessage('O limite da consulta não deve estar vazio')
				.isInt({ min: 1, max: 10 })
				.withMessage('O valor limite deve ser um número e entre 1-10'),
			query('offset')
				.optional()
				.isNumeric()
				.withMessage('O valor deve ser numérico'),
		];
	}
	checkIdParam() {
		return [
			param('id')
				.notEmpty()
				.withMessage('O valor não deve estar vazio')
				.isUUID(4)
				.withMessage('O valor não deve estar vazio'),
		];
	}
}

export default new ContratoValidator();