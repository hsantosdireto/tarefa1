import { Model, DataTypes, UUIDV4 } from 'sequelize';
import db from '../src/db/sequelize';

class contrModel extends Model {
    public id: number;
    public numerocontrato: number;
    public empresa: string;
    public nomedevedor: string;
    public statuscontrato: boolean;
    public createdAt?: Date;
    public updatedAt?: Date;
}

contrModel.init({
    id: {
        type: DataTypes.UUID,
        defaultValue: UUIDV4,
        autoIncrement: true,
        primaryKey: true,
    },
    numerocontrato: {
        type: DataTypes.INTEGER,
        allowNull: false,
        unique: true,
    },
    empresa: {
        type: DataTypes.STRING,
        allowNull: false
    },
    nomedevedor: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    statuscontrato: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        field: 'createdat',
        defaultValue: DataTypes.NOW,
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        field: 'updatedat',
        defaultValue: DataTypes.NOW,
    }},
	{
		sequelize: db,
		tableName: 'contratos'
	}
);

export default contrModel;
